--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bloodtest; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE bloodtest (
    id integer NOT NULL,
    hemoglobin integer,
    erythrocytes integer,
    volumeoferythrocytes integer,
    concentrationoferythrocytes integer
);


ALTER TABLE bloodtest OWNER TO test;

--
-- Name: patient; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE patient (
    id integer NOT NULL,
    firstname character varying(255),
    lastname character varying(255),
    fathername character varying(255),
    snils integer,
    birthday timestamp without time zone
);


ALTER TABLE patient OWNER TO test;

--
-- Name: tests_to_pacients; Type: TABLE; Schema: public; Owner: test; Tablespace: 
--

CREATE TABLE tests_to_pacients (
    id integer NOT NULL,
    bloodtestid integer NOT NULL
);


ALTER TABLE tests_to_pacients OWNER TO test;

--
-- Data for Name: bloodtest; Type: TABLE DATA; Schema: public; Owner: test
--

COPY bloodtest (id, hemoglobin, erythrocytes, volumeoferythrocytes, concentrationoferythrocytes) FROM stdin;
1	434343	443434	4343443	434333
\.


--
-- Data for Name: patient; Type: TABLE DATA; Schema: public; Owner: test
--

COPY patient (id, firstname, lastname, fathername, snils, birthday) FROM stdin;
1	hanyuu	furude	hiamizava	12345	2016-01-01 00:00:00
\.


--
-- Data for Name: tests_to_pacients; Type: TABLE DATA; Schema: public; Owner: test
--

COPY tests_to_pacients (id, bloodtestid) FROM stdin;
1	1
\.


--
-- Name: bloodtest_pkey; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY bloodtest
    ADD CONSTRAINT bloodtest_pkey PRIMARY KEY (id);


--
-- Name: patient_pkey; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY patient
    ADD CONSTRAINT patient_pkey PRIMARY KEY (id);


--
-- Name: tests_to_pacients_pkey; Type: CONSTRAINT; Schema: public; Owner: test; Tablespace: 
--

ALTER TABLE ONLY tests_to_pacients
    ADD CONSTRAINT tests_to_pacients_pkey PRIMARY KEY (id, bloodtestid);


--
-- Name: fkn4fxmyfi7mam696uvehkycctk; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY tests_to_pacients
    ADD CONSTRAINT fkn4fxmyfi7mam696uvehkycctk FOREIGN KEY (bloodtestid) REFERENCES bloodtest(id);


--
-- Name: fkscu5onskx471gyx6wreo45lx9; Type: FK CONSTRAINT; Schema: public; Owner: test
--

ALTER TABLE ONLY tests_to_pacients
    ADD CONSTRAINT fkscu5onskx471gyx6wreo45lx9 FOREIGN KEY (id) REFERENCES patient(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

