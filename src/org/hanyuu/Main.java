package org.hanyuu;
import org.hanyuu.soap.endpoints.SoapService;
import org.hanyuu.dao.HibernateSessionFactory;
import javax.xml.ws.Endpoint;
import org.hibernate.Session;

class Main{
  public static void main(String[] argv) {
    Session session = HibernateSessionFactory.getSessionFactory().openSession();//Init DB fields
    session.close();
    Object implementor = new SoapService();
    String address = "http://localhost:9000/soap";
    Endpoint.publish(address, implementor);
  }
}
