package org.hanyuu.dao;

/**
 * Created by Hanyuusha on 17.01.2017.
 */
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;

import org.hanyuu.models.BloodTest;
import org.hanyuu.models.Patient;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;
import java.util.Set;
import org.hibernate.Transaction;
import javax.persistence.NoResultException;
public class SoapDAO
{
    private static final Logger log = Logger.getLogger(SoapDAO.class.getCanonicalName());

    public Patient getPatientById(int id) {
        log.log(Level.INFO, "[SoapDAO.getPacient] Try to get Patient with id:{0}", new Object[]{id});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();

        try {
            Patient patient = (Patient)session.createQuery("from Patient WHERE id=:id").setParameter("id",id).getSingleResult();

            return patient;

        }catch (NoResultException nre){

            log.log(Level.WARNING, "[SoapDAO.getPacient] Try to get Patient with id:{0}, Patient not found!", new Object[]{id});

        }finally {

            session.close();
        }
        return null;

    }

    public BloodTest getBloodTestById(int id) {
        log.log(Level.INFO, "[SoapDAO.getBloodTestById] Try to get BloodTest with id:{0}", new Object[]{id});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();


        try {
            BloodTest bloodTest = (BloodTest)session.createQuery("from BloodTest WHERE id=:id").setParameter("id",id).getSingleResult();

            return bloodTest;

        }catch (NoResultException nre){

            log.log(Level.WARNING, "[SoapDAO.getBloodTestById] Try to get BloodTest with id:{0}, BloodTest not found!", new Object[]{id});

        }finally {

            session.close();
        }

        return null;

    }

    /*public boolean deleteBloodTestById(int id) {
        log.log(Level.INFO, "[SoapDAO.deleteBloodTestById] Try to delete BloodTest with id:{0}", new Object[]{id});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        BloodTest bloodTest;
        try {
            bloodTest = (BloodTest)session.createQuery("from BloodTest WHERE id=:id").setParameter("id",id).uniqueResult();

            try{

                session.delete(bloodTest);

                transaction.commit();

                log.log(Level.INFO, "[SoapDAO.deleteBloodTestById] Deleted",new Object[]{});


            }catch (Exception e){

                log.log(Level.WARNING, "[SoapDAO.deleteBloodTestById] Exception: {0}",new Object[]{e.getMessage()});
                e.printStackTrace();
                transaction.rollback();

                return false;

            }finally {

                session.close();
            }
            return true;

        }catch (NoResultException nre){

            log.log(Level.WARNING, "[SoapDAO.getBloodTestById] Try to delete BloodTest with id:{0}, BloodTest not found!", new Object[]{id});
        }
        return false;

    }*/

    public boolean deletePatientById(int id) {
        log.log(Level.INFO, "[SoapDAO.deletePatientById] Try to delete BloodTest with id:{0}", new Object[]{id});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        Patient patient = null;
        try {
            patient =  (Patient)session.createQuery("from Patient WHERE id=:id").setParameter("id",id).getSingleResult();

            try{

                session.delete(patient);

                transaction.commit();

                log.log(Level.INFO, "[SoapDAO.deletePatientById] Deleted",new Object[]{});
            }catch (Exception e){

                log.log(Level.WARNING, "[SoapDAO.deletePatientById] Exception: {0}",new Object[]{e.getMessage()});
                transaction.rollback();
                return false;

            }finally {

                session.close();
            }
            return true;
        }catch (NoResultException nre){

            log.log(Level.WARNING, "[SoapDAO.deletePatientById] Try to delete BloodTest with id:{0}, BloodTest not found!", new Object[]{id});
            return false;
        }


    }
    public boolean updateBloodTestById(int id,Date testDate, int hemoglobin, int erythrocytes, int volumeOfErythrocytes,int concentrationOfErythrocytes) {
            log.log(Level.INFO, "[SoapDAO.updateBloodTestById] Try to update BloodTest Id:{0}, testDate:{1}, hemoglobin:{2}, erythrocytes:{3}, volumeOfErythrocytes:{4}, concentrationOfErythrocytes:{5}",
                    new Object[]{id,testDate,hemoglobin,erythrocytes,volumeOfErythrocytes,concentrationOfErythrocytes});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        BloodTest bloodTest = null;
        try {
            bloodTest = (BloodTest)session.createQuery("from BloodTest WHERE id=:id").setParameter("id",id).getSingleResult();

            bloodTest.setTestDay(testDate);

            bloodTest.setHemoglobin(hemoglobin);

            bloodTest.setErythrocytes(erythrocytes);

            bloodTest.setConcentrationOfErythrocytes(concentrationOfErythrocytes);

            bloodTest.setVolumeOfErythrocytes(volumeOfErythrocytes);


            try{

                session.save(bloodTest);

                transaction.commit();

                log.log(Level.INFO, "[SoapDAO.updateBloodTestById] Saved",new Object[]{});

            }catch (Exception e){

                log.log(Level.WARNING, "[SoapDAO.updateBloodTestById] Exception: {0}",new Object[]{e.getMessage()});

                transaction.rollback();
                return false;

            }finally {

                session.close();
            }
            return true;

        }catch (NoResultException nre){

            log.log(Level.WARNING, "[SoapDAO.updateBloodTestById] Try to update BloodTest with id:{0}, BloodTest not found!", new Object[]{id});
            return false;

        }


    }

    public Patient createPatient(int snils, String firstName, String lastName, String fatherName,Date birthday) {
        log.log(Level.INFO, "[SoapDAO.createPacient] Try to create  Patient snils:{0}, firstName:{1}, lastName:{2}, fatherName:{3}, birthday:{4}",
                new Object[]{snils,firstName,lastName,fatherName,birthday});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();

        Patient pacient = new Patient();

        pacient.setBirthday(birthday);

        pacient.setFatherName(fatherName);

        pacient.setFirstName(firstName);

        pacient.setLastName(lastName);

        pacient.setSnils(snils);

        Transaction transaction = session.beginTransaction();

        try{

            session.save(pacient);

            transaction.commit();

            log.log(Level.INFO, "[SoapDAO.createPacient] Saved",new Object[]{});
            return pacient;
        }catch (Exception e){

            log.log(Level.WARNING, "[SoapDAO.createPacient] {0}",new Object[]{e.getMessage()});

            transaction.rollback();
        }finally {

           session.close();
        }
        return pacient;

    }

    public boolean updatePacient(int id,int snils, String firstName, String lastName, String fatherName,Date birthday) {
        log.log(Level.INFO, "[SoapDAO.updatePacient] Try to update  Patient snils:{0}, firstName:{1}, lastName:{2}, fatherName:{3}, birthday:{4}",
                new Object[]{snils,firstName,lastName,fatherName,birthday});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Patient patient  = null;
        try {
            patient = (Patient) session.createQuery("from Patient WHERE id=:id").setParameter("id",id).getSingleResult();
            try{
                patient.setBirthday(birthday);

                patient.setFatherName(fatherName);

                patient.setFirstName(firstName);

                patient.setLastName(lastName);

                patient.setSnils(snils);

                session.save(patient);

                transaction.commit();

                log.log(Level.INFO, "[SoapDAO.updatePacient] Saved",new Object[]{});

                return true;

            }catch (Exception e){

                log.log(Level.WARNING, "[SoapDAO.updatePacient] Exception: {0}",new Object[]{e.getMessage()});

                transaction.rollback();

                return false;

            }finally {

                session.close();
            }

        }catch (NoResultException nre){

            log.log(Level.WARNING, "[SoapDAO.updatePacient] Try to update Pacient with id:{0}, Pacient not found!", new Object[]{id});
            return false;

        }
    }

    public BloodTest createBloodTest(int id,Date testDate, int hemoglobin, int erythrocytes, int volumeOfErythrocytes,int concentrationOfErythrocytes) {
        log.log(Level.INFO, "[SoapDAO.createBloodTest] Try to create BloodTest patientId:{0}, testDate:{1}, hemoglobin:{2}, erythrocytes:{3}, volumeOfErythrocytes:{4}, concentrationOfErythrocytes:{5}",
                new Object[]{id,testDate,hemoglobin,erythrocytes,volumeOfErythrocytes,concentrationOfErythrocytes});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();

        BloodTest bloodTest=null;

        try {
            Patient patient = (Patient) session.createQuery("from Patient WHERE id=:id").setParameter("id",id).getSingleResult();
            try{
                bloodTest = new BloodTest();


                bloodTest.setConcentrationOfErythrocytes(concentrationOfErythrocytes);

                bloodTest.setErythrocytes(erythrocytes);

                bloodTest.setVolumeOfErythrocytes(volumeOfErythrocytes);

                bloodTest.setHemoglobin(hemoglobin);

                bloodTest.setTestDay(testDate);

                patient.getBloodTests().add(bloodTest);

                session.save(patient);

                session.save(bloodTest);

                transaction.commit();

                log.log(Level.INFO, "[SoapDAO.createBloodTest] Saved",new Object[]{});
                return bloodTest;
            }catch (Exception e){

                transaction.rollback();

                log.log(Level.WARNING, "[SoapDAO.createBloodTest] Error:{0}",new Object[]{e.getMessage()});

                return null;

            }finally {

                session.close();
            }

        }catch (NoResultException nre){

            log.log(Level.WARNING, "[SoapDAO.createBloodTest] Try to create BloodTest for Patient with id:{0}, Pacient not found!", new Object[]{id});
            return null;
        }

    }

    public Set<BloodTest> getBloodTestsByPacientId(int id){
        log.log(Level.INFO, "[SoapDAO.getBloodTestsByPacientId] Try to getBloodTestsByPacientId patient with id {0}", new Object[]{id});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();

        Set<BloodTest> bloodTests = null;

            try {

                 Patient patient = (Patient) session.createQuery("from Patient WHERE id=:id").setParameter("id",id).getSingleResult();

                 bloodTests =  patient.getBloodTests();

            }catch (NoResultException nre){

                log.log(Level.WARNING, "[SoapDAO.getBloodTestsByPacientId] Try to get BloodTests of Pacient with id:{0}, Pacient not found!", new Object[]{id});

            }finally {

                session.close();
            }

        return  bloodTests;

    }

    public boolean deleteBloodTestsByPacientIdAndBloodTestId(int pacientId,int bloodTestId){
        log.log(Level.INFO, "[SoapDAO.deleteBloodTestsByPacientIdAndBloodTestId] Try to getBloodTestsByPacientId Patient with id {0} and BloodTest {1}", new Object[]{pacientId,bloodTestId});

        Session session = HibernateSessionFactory.getSessionFactory().openSession();

        Transaction transaction = session.beginTransaction();


        try {
            BloodTest bloodTest = (BloodTest)session.createQuery("from BloodTest WHERE id=:id").setParameter("id",bloodTestId).getSingleResult();

            try {
                Patient patient = (Patient) session.createQuery("from Patient WHERE id=:id").setParameter("id",pacientId).getSingleResult();

                patient.getBloodTests().remove(bloodTest);
                try{

                    session.save(patient);
                    session.delete(bloodTest);
                    transaction.commit();

                    log.log(Level.INFO, "[SoapDAO.deleteBloodTestsByPacientIdAndBloodTestId] Deleted",new Object[]{});

                    return true;
                }catch (Exception e){

                    transaction.rollback();
                    log.log(Level.WARNING, "[SoapDAO.deleteBloodTestsByPacientIdAndBloodTestId] Error:{0}",new Object[]{e.getMessage()});

                }finally {

                    session.close();
                }
            }catch (NoResultException nre){
                log.log(Level.WARNING, "[SoapDAO.deleteBloodTestsByPacientIdAndBloodTestId] Try to delete BloodTest with id:{0} from Pacient with id:{0}, Pacient not found!", new Object[]{bloodTestId,pacientId});
            }

        }catch (NoResultException nre){

            log.log(Level.WARNING, "[SoapDAO.deleteBloodTestsByPacientIdAndBloodTestId] Try to delete BloodTest with id:{0} from Pacient with id:{0}, BloodTest not found!", new Object[]{bloodTestId,pacientId});

        };
        return false;
    }

}
