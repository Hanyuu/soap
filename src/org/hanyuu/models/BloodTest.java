package org.hanyuu.models;
import java.util.Date;
/**
 * Created by Hanyuusha on 17.01.2017.
 */
public class BloodTest
{
    private int BloodTestId;

    private Date testDay;

    private int hemoglobin;

    private int erythrocytes;

    private int volumeOfErythrocytes;

    private int concentrationOfErythrocytes;

    public Date getTestDay() {
        return testDay;
    }

    public void setTestDay(Date testDay) {
        this.testDay = testDay;
    }

    public int getHemoglobin() {
        return hemoglobin;
    }

    public void setHemoglobin(int hemoglobin) {
        this.hemoglobin = hemoglobin;
    }

    public int getErythrocytes() {
        return erythrocytes;
    }

    public void setErythrocytes(int erythrocytes) {
        this.erythrocytes = erythrocytes;
    }

    public int getVolumeOfErythrocytes() {
        return volumeOfErythrocytes;
    }

    public void setVolumeOfErythrocytes(int volumeOfErythrocytes) {
        this.volumeOfErythrocytes = volumeOfErythrocytes;
    }

    public int getConcentrationOfErythrocytes() {
        return concentrationOfErythrocytes;
    }

    public void setConcentrationOfErythrocytes(int concentrationOfErythrocytes) {
        this.concentrationOfErythrocytes = concentrationOfErythrocytes;
    }

    public int getId() {
        return BloodTestId;
    }

    public void setId(int id) {
        this.BloodTestId = id;
    }
}
