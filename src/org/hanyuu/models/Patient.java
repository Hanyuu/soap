/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.hanyuu.models;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;

/**
 *
 * @author Hanyuusha
 */
public class Patient 
{
    private int snils;

    private int id;

    private String firstName;
    
    private String lastName;
    
    private String fatherName;
    
    private Date  birthday;

    private Set<BloodTest> bloodTests = new HashSet<BloodTest>();

    /**
     * @return the snils
     */
    public int getSnils() {
        return snils;
    }

    /**
     * @param snils the snils to set
     */
    public void setSnils(int snils) {
        this.snils = snils;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the fatherName
     */
    public String getFatherName() {
        return fatherName;
    }

    /**
     * @param fatherName the fatherName to set
     */
    public void setFatherName(String fatherName) {
        this.fatherName = fatherName;
    }

    /**
     * @return the birthday
     */
    public Date getBirthday() {
        return birthday;
    }

    /**
     * @param birthday the birthday to set
     */
    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<BloodTest> getBloodTests() {
        return bloodTests;
    }

    public void setBloodTests(Set<BloodTest> bloodTests) {
        this.bloodTests = bloodTests;
    }
}
