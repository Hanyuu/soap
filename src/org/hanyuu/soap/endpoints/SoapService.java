package org.hanyuu.soap.endpoints;

/**
 * Created by Hanyuusha on 17.01.2017.
 */

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.WebParam;
import java.util.Set;
import java.util.logging.Logger;
import org.hanyuu.dao.SoapDAO;
import org.hanyuu.models.BloodTest;
import org.hanyuu.models.Patient;
import org.hanyuu.soap.responses.Response;

import java.util.Date;

/**
 * Created by Hanyuusha on 17.01.2017.
 */
@WebService()
public class SoapService  {
    private static final Logger log = Logger.getLogger(SoapService.class.getCanonicalName());
    private final SoapDAO soapDao = new SoapDAO();

    @WebMethod
    public Patient getPatient(@WebParam(name = "patientId")int patientId) {
        if(patientId <0){
            return null;
        }
        return this.soapDao.getPatientById(patientId);
    }

    @WebMethod
    public Patient createPatient(@WebParam(name = "snils")int snils,
                                 @WebParam(name = "firstName")String firstName,
                                 @WebParam(name = "lastName")String lastName,
                                 @WebParam(name = "fatherName")String fatherName,
                                 @WebParam(name = "birthday")Date birthday) {
        Response response = new Response();
        if(snils<0||firstName == null||firstName==null||lastName==null||fatherName==null||birthday==null){
            return null;
        }
        return this.soapDao.createPatient(snils,firstName,lastName,fatherName,birthday);

    }

    @WebMethod
    public Response updatePatient(@WebParam(name = "id")int id,
                                 @WebParam(name = "snils")int snils,
                                 @WebParam(name = "firstName")String firstName,
                                 @WebParam(name = "lastName")String lastName,
                                 @WebParam(name = "fatherName")String fatherName,
                                 @WebParam(name = "birthday")Date birthday) {
        Response response = new Response();
        if(snils<0||firstName == null||firstName==null||lastName==null||fatherName==null||birthday==null){
            response.setMessage("Not all fields correct");
            response.setError(true);
            return response;
        }
        boolean result = this.soapDao.updatePacient(id,snils,firstName,lastName,fatherName,birthday);
        if(result){
            response.setMessage("Patient updeted");
            response.setError(false);
            return response;
        }else {
            response.setMessage("Patient not updated");
            response.setError(true);
            return response;
        }
    }

    @WebMethod
    public Response deletePatientById(@WebParam(name = "id")int id) {
        Response response = new Response();
        if(id < 1){
            response.setMessage("Patient not found or id less that one");
            response.setError(true);
            return response;
        }
        boolean result = this.soapDao.deletePatientById(id);
        if(result){
            response.setMessage("Patient deleted");
            response.setError(false);
            return response;
        }else {
            response.setMessage("Patient not deleted");
            response.setError(true);
            return response;
        }
    }


    @WebMethod
    public BloodTest createBloodTest(@WebParam(name = "patientId")int patientId,
                                     @WebParam(name = "testDay")Date testDay,
                                     @WebParam(name = "hemoglobin") int hemoglobin,
                                     @WebParam(name = "erythrocytes") int erythrocytes,
                                     @WebParam(name = "volumeOfErythrocytes") int volumeOfErythrocytes,
                                     @WebParam(name = "concentrationOfErythrocytes")int concentrationOfErythrocytes) {
        Patient patient = this.soapDao.getPatientById(patientId);
        if (patient == null||patientId<0){
            return null;
        }
        return this.soapDao.createBloodTest(patientId,testDay,hemoglobin,erythrocytes,volumeOfErythrocytes,concentrationOfErythrocytes);

    }

    @WebMethod
    public BloodTest getBloodTestById(@WebParam(name = "id")int id) {
        if(id < 1){
            return null;
        }
        return this.soapDao.getBloodTestById(id);
    }
    @WebMethod
    public Response updateBloodTestById(@WebParam(name = "id")int id,
                                         @WebParam(name = "testDay")Date testDay,
                                         @WebParam(name = "hemoglobin") int hemoglobin,
                                         @WebParam(name = "erythrocytes") int erythrocytes,
                                         @WebParam(name = "volumeOfErythrocytes") int volumeOfErythrocytes,@

                                         WebParam(name = "concentrationOfErythrocytes")int concentrationOfErythrocytes) {
        Response response = new Response();
        if(id < 1){
            response.setMessage("Id cannot be less that one");
            response.setError(true);
            return response;
        }
        boolean result =  this.soapDao.updateBloodTestById(id,testDay,hemoglobin,erythrocytes,volumeOfErythrocytes,concentrationOfErythrocytes);
        if(result){
            response.setMessage("Record updated");
            response.setError(false);
            return response;
        }else {
            response.setMessage("Record not updated");
            response.setError(true);
            return response;
        }

    }




    @WebMethod
    public Set<BloodTest> getBloodTestsByPacientId(@WebParam(name = "patientId")int patientId) {
         if (patientId <0 ){
            return null;
        }

        return this.soapDao.getBloodTestsByPacientId(patientId);
    }

    @WebMethod
    public Response deleteBloodTestsByPacientIdAndBloodTestId(@WebParam(name = "patientId")int patientId,@WebParam(name = "bloodTestId")int bloodTestId) {
        Response response = new Response();
        if (patientId < 0) {
            response.setMessage("Id cannot be less that one");
            response.setError(true);
            return response;
        }

        boolean result = this.soapDao.deleteBloodTestsByPacientIdAndBloodTestId(patientId, bloodTestId);
        if (result) {
            response.setMessage("Record deleted");
            response.setError(false);
            return response;
        } else {
            response.setMessage("Cannot delete record");
            response.setError(true);
            return response;
        }

    }
}