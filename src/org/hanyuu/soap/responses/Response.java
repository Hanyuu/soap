package org.hanyuu.soap.responses;

/**
 * Created by Hanyuusha on 25.01.2017.
 */
public class Response
{
    private String message;

    private boolean isError;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }
}
