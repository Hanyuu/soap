package org.hanyuu.tests;
import static org.junit.Assert.*;

import org.hanyuu.soap.endpoints.SoapService;
import org.hanyuu.soap.responses.Response;
import org.junit.Test;
import org.hanyuu.soap.*;
import org.hanyuu.models.Patient;
import org.hanyuu.models.BloodTest;
import java.util.Date;
/**
 * Created by Hanyuusha on 25.01.2017.
 */
public class Tests {

    private final SoapService  soap = new SoapService();
    @Test
    public void createAndGetPatient(){
        Date d = new Date();
        Patient patient1 = this.soap.createPatient(11,"FN","LN","PN",d);
        Patient patient2 = this.soap.getPatient(patient1.getId());
        assertEquals(patient1.getId(),patient2.getId());
    }
}
